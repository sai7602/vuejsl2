const Card = {
    props: {
        anime: {
            type: Object,
            required: true
        },
        size: {
            type: Number,
            default: 4
        }
    },
    template: `
    <div :class="'col-'+size">
        <div class="card">
            <img :src="anime.image_url" :alt="anime.title" class="card-img-top">
            <div class="card-body">
                <div class="card-title">{{ anime.title }}</div>
                <div class="card-text">{{ anime.episodes }}</div>
            </div>
            <div class="card-footer">
                <a :href="anime.url" class="card-link">go</a>
            </div>
        </div>
    </div>
    `
}