const OnAbsence = {
    props: ['text'],
    template: `<div class="error">{{ text }}</div>`
}