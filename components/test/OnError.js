const OnError = {
    props: ['text'],
    template: `<div class="error">{{ text }}</div>`
}