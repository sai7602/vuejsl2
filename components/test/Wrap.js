const Wrap = {
    components: {
        card: Card,
        preloader: Preloader,
        'on-absence': OnAbsence,
        'on-error': OnError
    },
    props: ['title'],
    data() {
        return {
            anime: [],
            isAnimeLoaded: false,
            isAnimeError: false
        }
    },
    created() {
        this.getAnime();
    },
    methods: {
        getAnime() {
            const self = this;
            fetch('https://api.jikan.moe/v3/top/anime')
                .then(resp => resp.json())
                .then(data => {
                    console.log(data.top);
                    self.anime = data.top;
                    self.isAnimeLoaded = true
                })
                .catch(err => {
                    self.isAnimeLoaded = true;
                    self.isAnimeError = true;
                    console.log(err)
                })
        }
    },
    template: `
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>{{ title }}</h1>
            </div>
            <preloader v-show="!isAnimeLoaded"></preloader>
            <on-error v-if="isAnimeError" text="error"></on-error>
            <on-absence v-if="isAnimeLoaded && (anime.length && anime.length < 1)" text="no data"></on-absence>
            <template v-else>
               <card v-for="item in anime" :key="item.mal_id" :anime="item"></card>
            </template>
        </div>
    </div>
    `
}