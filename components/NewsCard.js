const newsCard = {
    props: {
        news: {
            type: Object,
            required: true
        },
        cardSize: {
            type: Number,
            default: 4
        }
    },
    data() {
        return {
            defImg: 'https://cdn.ymaws.com/www.itsmfusa.org/resource/resmgr/images/more_images/news-3.jpg'
        }
    },
    methods: {
        getImg() {
            return this.news.img ? this.news.img : this.defImg
        }
    },
    template: `
        <div :class="'col-'+cardSize">
            <div class="card">
                <img :src="getImg()" :alt="news.title" class="card-img-top">
                <div class="card-body">
                    <div class="card-title">{{ news.title }}</div>
                    <div class="card-text">{{ news.desc }}</div>
                </div>
                <div class="card-footer">
                    <a :href="news.link" class="card-link">more</a>
                </div>
            </div>
        </div>
    `
}