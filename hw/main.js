// Прелоадер
Vue.component('is-data-load', {
  template: '<div class="center">Please wait. Loading...</div>'
});
// Помилка
Vue.component('err', {
    template: '<div class="center-text">Упс... Щось пішло не так.</div>'
});
// Кнопка
Vue.component('btn', {
    props: {name: String},
    template: '<button>{{name}}</button>',
});
// Помилка пошуку
Vue.component('not-found-item', {
    template: '<div class="center-text">Ми нічого не змогли знайти за вашими результатами.</div>'
});
// Шаборн виводу карточок
Vue.component('anime-cards', {
  props: ['item'],
  template: 
  `
  <div class="col-md-3">
      <div class="card">
          <img :src="item.image_url" :alt="item.title" class="card-img-top">
          <div class="card-body">
              <div class="card-title">Назва: {{item.title}}</div>
              <div class="card-text">Тривалість: {{item.score}}</div>
              <div class="card-type">Тип: {{item.type}}</div>
          </div>
          <div class="card-footer">
          <a :href="item.url" class="btn btn-primary">more</a>
          </div>
      </div>
  </div>
  `
  });
//   ВИбір фільтру по типу
  Vue.component('type-of-movie', {
    props: ['item'],
    template: `
    <div><label><input type="checkbox" @click="clickName">{{item.name}}</label>
     </div>
`,
    methods: {
        clickName() {
            this.item.value = !this.item.value;
        }
    }
});


const newApp = new Vue({
  el: '#app',
  data: {
    animeCards: [],
    loading: true,
    err: false,
    search: '',
    typeOfMovie: [
        { name: 'TV', value: false },
        { name: 'Movie', value: false },
        { name: 'OVA', value: false }
    ],
    animeCardsLength: 'number',
    deleteAnimeCards: false,
    loadAnimeCards: false
  },
  created: function() {
      this.getMovies();
  },
  computed:{
    filteringByType: function(){
          let self = this;
          let animeCards = self.animeCards;
          let search = self.search.toLowerCase();
          animeCards =  animeCards.filter(function (el) {
              let valid = false;
              if(search==='') valid = true;
              else {
                  valid = el.title.toLowerCase().indexOf(search) !== -1;
                  console.log(el.title.toLowerCase().indexOf(search))
                }
              self.typeOfMovie.forEach((item) => {
                  if(item.value) {
                      if(valid && el.type !== item.name) valid = false;
                  };
              });
              return valid;
            });
          self.animeCardsLength = animeCards.length;
          return animeCards;
      },
  },
  methods: {
      deleteAllCards: function() {
          this.animeCards = [];
      },
      getMovies: function() {
          let self = this;
          self.loading = true;
          setTimeout(() => {
              self.loading = false
          }, 1500);
          fetch('https://api.jikan.moe/v3/top/anime')
              .then(resp => resp.json())
              .then(list => {
                  if('top' in list) {
                      self.animeCards = list.top;
                      this.filteringByType;
                  } else {
                      self.err = true;
                  }
              })
              .catch(() => self.err = true);
      },
  },
});


