Vue.component('card', {
props: {
item: {
    type: Object,
    required: true,

    }
},
data: function () {
    return {
        defImg: 'https://m.files.bbci.co.uk/modules/bbc-morph-news-waf-page-meta/2.5.2/bbc_news_logo.png'
    }
},
methods: {
    imgValid(){
       return  this.item.img ? this.item.img : this.defImg
    }
},
template: 
`
<div class="col-4">
    <div class="card">
        <img :src="imgValid()" :alt="item.title" class="card-img-top">
        <div class="card-body">
            <div class="card-title">{{item.title}}</div>
            <div class="card-text">{{item.desc}}</div>
        </div>
        <div class="card-footer">
        <a :href="item.link" class="card-link">more</a>
        </div>
    </div>
</div>
`
});

